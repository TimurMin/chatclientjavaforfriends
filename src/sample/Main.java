package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.net.Socket;
import java.text.ParseException;

public class Main extends Application {
    PrintWriter writer;
    Controller controller = new Controller();
    Socket socket;
    BufferedReader reader;


    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Avalon chat");
        primaryStage.setScene(new Scene(root, 484, 288));
        primaryStage.show();
        setupSocket();
        Thread readerThread = new Thread(new IncomingReader());
        readerThread.start();
      //  controller.textMessage.setText("Hi");
    }


    public static void main(String[] args) {
        launch(args);
    }

    public void sendMessage(String text) {
        try {
            JSONObject resultJson = new JSONObject();
            resultJson.put("login", text);

            writer.println(resultJson.toString());
            writer.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setupSocket(){
        try{
            socket = new Socket("192.168.20.254", 4242);
            InputStreamReader streamReader = new InputStreamReader(socket.getInputStream());
            reader = new BufferedReader(streamReader);
            writer = new PrintWriter(socket.getOutputStream());
            System.out.println("networking established");

        }catch (IOException ex){ex.printStackTrace();}
    }

    public class IncomingReader implements Runnable{
        public void run(){
            String message;
            try{
                while ((message = reader.readLine())!=null){
                    JSONParser parser = new JSONParser();
                    JSONObject jsonObj = null;
                    jsonObj = (JSONObject) parser.parse(message);
                    System.out.println(jsonObj.get("login"));
                    controller.messagesArea.appendText((String) jsonObj.get("login"));
                }
            }catch (Exception ex){ex.printStackTrace();}
        }
    }
}