package sample;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.json.JSONException;

import java.awt.*;

public class Controller {

    @FXML
    TextField textMessage;

    @FXML
    TextArea messagesArea;

    @FXML
    public void actionSendButton() throws JSONException {
        Main main = new Main();
        main.sendMessage(textMessage.getText());
    }

}
